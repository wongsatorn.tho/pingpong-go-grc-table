package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/wongsatorn.tho/pingpong-go-grc-table/pkg/server"
)

func main() {
	flag.Parse()

	if err := server.RunServer(); err != nil {
		fmt.Printf("%v", err)
		os.Exit(1)
	}
}
