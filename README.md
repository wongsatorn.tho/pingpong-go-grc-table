# go-boilerplate01 v2

![release](https://img.shields.io/badge/release-v0.2.1-blue)
![coverage](https://img.shields.io/badge/coverage-89.1%25-brightgreen)
![build](https://img.shields.io/badge/build-passing-brightgreen) 
![goguild](https://img.shields.io/badge/goguild_certified-passing-green) 
![rating](https://img.shields.io/badge/rating-★★★☆☆-red) 

__go-boilerplate01__ is a project for create and automate generated code gRPC, Swagger API service in golang.
You may use this project as boilerplate for start a new microservice without write the same initial code for every service.

Please feel free to fork and create another boilerplate if you think this one cannot solve your problem.
You may also create issue in this project if you want us to improve anything.

*__this boilerplate v0.2.x has upgraded to gRPC-Gateway v2__

# Table of Contents
- [Quick start](#quick-start)
- [Tutorials](#tutorials)
- [How does this work?](#how-does-this-work)
- [Project Structure](#project-structure)
- [Getting Started](#getting-started)
    * [Prerequisites](#prerequisites)
        + [Installing prerequisite tools](#installing-prerequisite-tools)
        + [Verify PATH](#verify-path)
        + [First time setup project](#first-time-setup-project)
    * [Running the test](#running-the-test)
    * [Regenerate code](#regenerate-code)
    * [Regenerate code, then fmt, then vet, then test](#regenerate-code-then-fmt-then-vet-then-test)
    * [Upgrade tools & libraries](#upgrade-tools--libraries)
    * [Deployment](#deployment)
- [All Makefile commands](#all-makefile-commands)
- [How to implement API?](#how-to-implement-api)
- [How to customize HOST/PORT?](#how-to-customize-hostport)
- [How to customize application configuration?](#how-to-customize-application-configuration)
- [CHANGE LIST](#change-list)
- [Good resources that helps create this project](#good-resources-that-helps-create-this-project)

# Quick start
If you want to run this project without edit Proto file or generate code,
then you can just run with:
- Run application locally with go
  ```sh
  make run
  ```
- Run application via docker command
  ```sh
  docker build -t go-boilerplate01 .
  docker run --rm -it -e PORT=8889 -p 8889:8889 go-boilerplate01
  ```
You can use your web browser to test application at http://127.0.0.1:8889/web


# Tutorials
We have tutorials on how this project works and how we create this project in Confluence below:
- [__Part 1__ : Create gRPC server from Protobuf](https://agilepm.kasikornbank.com:8444/x/YYK_Ag)
- [__Part 2__ : Generate REST endpoint and Swagger UI to gRPC server that support multiplex on the same port](https://agilepm.kasikornbank.com:8444/x/cIK_Ag)
- [__Part 3__ : Adding middleware, logging, recovery, request-id, graceful shutdown, health check](https://agilepm.kasikornbank.com:8444/x/moK_Ag)
- [__Part 4__ : Generate clients to REST and gRPC services from Swagger/Protobuf files](https://agilepm.kasikornbank.com:8444/x/X4O_Ag)
- __Part 5__ : Setup project to deploy to any PaaS, Heroku, GAE, CouldRun in one command

# How does this work?

![context diagram](go-boilerplate01.png)

This project requires server protobuf input file to generate gRPC server + REST server via Make command, then you can add only business logic.
We have put echo-service as initial example service, this service just only echo request back to requester.
You can also put client proto or swagger file to generate clients as well.

If you run gRPC server and REST server on the same port. 
This project will use [connection multiplex](https://github.com/soheilhy/cmux) to support multiple protocol on the same port.

# Project Structure
- `api/`: all API specifications (proto/swagger files)
    - `api/server/`: place to keep your server .proto file, there should be only one file represent your server API.
    - `api/client/`: place to store all your clients API, you can put either proto or swagger yaml or swagger json files here (support both swagger v2 and v3 format)
- `cmd/`: main application
    - `cmd/server/`: default main application to run local
    - `cmd/paas/`: default main application to run on PaaS platform (mainly different is env *PORT* variable)
- `pkg/`: application packages and generated code
    - `pkg/client/`: folder for placing auto-generated code; do not place your source code here, otherwise it will be removed by Makefile
    - `pkg/proto/`: folder for placing auto-generated protobuf server code; do not place your source code here, otherwise it will be removed by Makefile
    - `pkg/server/`: server initialization and handlers, where gRPC and REST endpoints are registered.
    - `pkg/service/`: services to be implemented. __You should put your services and business logic here.__
- `third_party/`: External helper tools, forked code and other 3rd party utilities (e.g., Swagger UI). In this boilerplate, we place google proto library files here.
- `tools/`: import tool dependencies for go module (ref: [tool dependencies](https://github.com/golang/go/wiki/Modules#how-can-i-track-tool-dependencies-for-a-module))
- `vendor/`: packages used in application
- `web/`: web application. 
    - `web/swaager-ui/`: swagger-ui [distribution](https://github.com/swagger-api/swagger-ui/tree/master/dist)

# Getting Started
These instructions will provide you the information to setup this project on your local machine.

Also, please note that this project's scripts are written for Unix-based OS. So some commands or Makefile will not work on Windows.
We are open for everyone to contribute to improve this project.

## Prerequisites
We use Protobuf and Swagger tools to auto generate code. Please make sure you already have the following software installed.
- [Go 1.15+](https://golang.org/dl/) 
- [Protocol Buffer Compiler](https://grpc.io/docs/protoc-installation/)

### Installing prerequisite tools
For MacOS use the following
```sh
brew install go
brew install protobuf
```

### Verify PATH
Verify that PATH environment variable is pointed to GOPATH/bin or GOBIN.  
This is because we need `go install` binary to works (e.g. protobuf code gen command)

For more information on this issue and golang official guide:
- https://github.com/golang/go/issues/23439#issuecomment-582483567
- https://golang.org/doc/gopath_code.html#GOPATH.

Example command (need to run only once)
```sh
export PATH=$PATH:$(go env GOPATH)/bin
```

### First time setup project
After you have installed all prerequisite tools and clone this project.
You may make sure everything working properly by.

1. Ensure go module download all requires libraries and tidy it.
    ```sh
    make tidy-vendor
    ```
2. Validate tools and commands. This command will show error if some tools is not installed and download it if possible.
    ```sh
    make install
    ```
3. Tried re-generate gRPC code and Swagger, you may need to use this command often during development.
    ```sh
    make gen
    ```
4. *(optional)* Build binary for run local
    ```sh
    make build
    ```

## Running the test
Run `Unit Test` and check code coverage by executing following command.
```sh
make test
```

## Regenerate code
When you made changes to the proto server file or added new clients spec (can be either protobuf client or json swagger).
You can regenerate server/client and swagger with the following command.
```sh
make gen
```

## Regenerate code, then fmt, then vet, then test
```sh
make all
```

## Upgrade tools & libraries
Sometimes you may want to upgrade all tools (e.g. brew, protoc, swagger, grpc-gateway).
You can do them all by run this command.
```sh
make upgrade
```

## Deployment
This program support deployment out-of-the-box for the following cloud PaaS: [Heroku](https://www.heroku.com/), [Google App Engine](https://cloud.google.com/appengine), [Google Cloud Run](https://cloud.google.com/run).
You may deploy on any other container-support provider using provided Dockerfile, it's just that above PaaS is free :)
- Deploy to **Heroku**:

  This project used [minimal default buildpack](https://aaronsmith.online/minimal-golang-heroku-app/) to deploy, so only file `Procfile` is needed to deploy.
  You can use Heroku's git and just push to deploy 
  ```sh
  git push
  ```
- Deploy to **Google App Engine**:

  This project provided `app.yaml` for deploy to GAE. You can use the following command to push and deploy.
  ```sh
  gcloud app deploy
  ```
- Deploy to **Google Cloud Run**:

  Cloud Run requires to build images to artifactory before able to deploy.
  Change `PROJECT-ID` into your google project id and then run command.
  ```sh
  gcloud builds submit --tag gcr.io/PROJECT-ID/cloudrun
  gcloud run deploy --platform managed --image gcr.io/PROJECT-ID/cloudrun
  ```

# All Makefile commands
You can see all Makefile commands by run `make help`
```sh
$ make help
```

# How to implement API?
1. Place proto or swagger files in `/api/server` and `/api/client`.  
   (Server folder must place only 1 proto file, while client folder can have as many files as you want)
1. Run `make gen` to generate server and clients code
1. Run `make tidy` to download any additional go modules required
1. Create your business logic services and unit tests in `/pkg/services`, you can see example from `echo-service.go` or `healthcheck-service.go`
1. Add service to gRPC server in `/pkg/server/server.go`, you can see example how to register Echo and Health service there
1. Run `make test` to verify unit test
1. Run `make run` to test run in localhost, the static website and swagger UI can be accessed from http://127.0.0.1:8889/web
1. You can customize middlewares or custom configs in `/pkg/server/server.go` and `/pkg/configs/config.go`

# How to customize HOST/PORT?
The default boilerplate will start gRPC server on PORT 9099 and REST server on PORT 8889.  
You can change by set flag PORT either in environment variable or args parameter.
- Environment variable:
  ```sh
  GRPC_PORT=9099
  REST_PORT=8889
  ```
- Args parameters:
  ```sh
  go run cmd/server/main.go --grpc-port=9099 --rest-port=8889
  ```

# How to customize application configuration?
This boilerplate use default `flag` package and [12-factors](https://www.gmarik.info/blog/2019/12-factor-golang-flag-package/) for custom application configurations.
You can set config via parameters or ENV.

The most used configurations are:
- `GRPC_PORT`- port to serve gRPC service (default 9099)
- `REST_PORT`- port to serve REST service (default 8889)
- `ALLOW_CORS`- set to true to allow [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
- `GRPC_HEALTHCLIENT_SERVERADDR`- Server Address to connect to gRPC Health service in Echo service example, default to `localhost:9099`
- `PETSTORE_URL`- URL of petstore service to be called in Echo service example, default to `http://petstore.swagger.io/v2`

You can find all configuration of this boilerplate in `pkg/configs/config.go`


# CHANGE LIST
- `v0.2.0`: upgrade library to use grpc-gateway v2
- `v0.1.1`: upgrade go modules version

# Good resources that helps create this project
- [Standard Go Project Layout](https://github.com/golang-standards/project-layout)
- [gRPC Basics Part 2: Rest and Swagger](https://levelup.gitconnected.com/grpc-basics-part-2-rest-and-swagger-53ec2417b3c4)
- [How to develop Go gRPC microservice with HTTP/REST endpoint, middleware, Kubernetes deployment, etc.](https://medium.com/@amsokol.com/tutorial-how-to-develop-go-grpc-microservice-with-http-rest-endpoint-middleware-kubernetes-daebb36a97e9)
- [go-swagger](https://goswagger.io/)
- [gRPC Gateway](https://github.com/grpc-ecosystem/grpc-gateway)
- [Go gRPC Middleware](https://github.com/grpc-ecosystem/go-grpc-middleware)
- [Muxing gRPC and HTTP traffic with grpc-gateway](https://www.clarifai.com/blog/muxing-together-grpc-and-http-traffic-with-grpc-gateway)
- [cmux: Connection multiplexer for GoLang](https://github.com/soheilhy/cmux)
- [Example of graceful shutdown with grpc healthserver * httpserver](https://gist.github.com/akhenakh/38dbfea70dc36964e23acc19777f3869)
- [Learn Go by writing tests: Structs, methods, interfaces & table driven tests](https://dev.to/quii/learn-go-by-writing-tests-structs-methods-interfaces--table-driven-tests-1p01)
- [Minimal Golang Heroku App](https://aaronsmith.online/minimal-golang-heroku-app/)
- [Heroku](https://www.heroku.com/)
- [Google App Engine](https://cloud.google.com/appengine)
- [Google Cloud Run](https://cloud.google.com/run)
- [Effective Go](https://golang.org/doc/effective_go.html)
- [Go wiki](https://github.com/golang/go/wiki)
