# Use the official Golang image to create a build artifact.
# https://hub.docker.com/_/golang
FROM golang:1.16-alpine as builder

# Create and change to the app directory.
WORKDIR /app

# Retrieve application dependencies using go modules.
# Allows container builds to reuse downloaded dependencies.
# comments these line to make vendor automatically compatible in go1.14+
# COPY go.* ./
# RUN go mod download

# Copy local code to the container image.
COPY . ./

# Build the binary.
# -mod=readonly ensures immutable go.mod and go.sum in container builds.
# -mod=vendor to not re-download vendor packages
# if set go1.14+ in go.mod, no need to add -mod flag since it detect vendor automatically
RUN CGO_ENABLED=0 GOOS=linux go build -v -o server ./cmd/paas/

# Use the official Alpine image for a lean production container.
# https://hub.docker.com/_/alpine
# https://docs.docker.com/develop/develop-images/multistage-build/#use-multi-stage-builds
FROM alpine
RUN apk add --no-cache ca-certificates

# Copy the binary to the production image from the builder stage.
COPY --from=builder /app/server /server
COPY --from=builder /app/web /web

# Go1.15 allow to put timezone data by import in main program
ENV TZ=Asia/Bangkok

# default port if no env REST_PORT, GRPC_PORT specific
EXPOSE 8889 9099

# Run the web service on container startup.
CMD ["/server"]

# == use the following command to deploy to Google Cloud ==
# PROJECT_ID=
# gcloud builds submit --tag gcr.io/$PROJECT_ID/cloudrun
# gcloud run deploy --platform managed --image gcr.io/$PROJECT_ID/cloudrun
# ---
# after deploy, it's better to clean container images to save disk space in cloud stoage
# gcloud container images list-tags gcr.io/$PROJECT_ID/cloudrun  --filter='-tags:*'  --format="get(digest)" --limit=10
# gcloud container images delete gcr.io/$PROJECT_ID/cloudrun@DIGEST --quiet
# ---one-line---
# gcloud container images list-tags gcr.io/$PROJECT_ID/cloudrun --filter='-tags:*' --format='get(digest)' --limit=unlimited | xargs -I {arg} gcloud container images delete  "gcr.io/$PROJECT_ID/cloudrun@{arg}" --quiet
# ---
# the log can also be list and change retention here:
# gcloud beta logging buckets update _Default --location=global --retention-days=7
# gcloud beta logging buckets describe _Default --location=global