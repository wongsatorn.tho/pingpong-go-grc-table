package configs

import (
	"flag"
	"os"
	"strconv"
	"time"

	"google.golang.org/grpc/keepalive"
)

// config for server related: port, timeout
type serverConfig struct {
	// port for gRPC service on HTTP/2
	GRPCPort string
	// port for REST service and swagger on HTTP/1.1
	RESTPort string
	// flag if we want to enable CORS, should disable on production private service
	AllowCORS bool
	// timeout in second to force stop server in case graceful hang, should be more than CmuxReadTimeout
	GracefulStopTimeout int
	// Read timeout in second for cmux to listen to request (note: need this parameter because bug in cmux https://github.com/soheilhy/cmux/issues/76)
	CmuxReadTimeout int
}

// config to use to connect to PetStore service
type petStoreClientConfig struct {
	// petstore use URL like http://petstore.swagger.io/v2 to connect to.
	URL string
}

// config to call gRPC HealthCheck in Echo service example
type gRPCHealthClientConfig struct {
	// gRPC use TCP Address like localhost:9099 to connect to.
	Addr string
}

// we declare default gRPC KeepAlive because this issue: https://github.com/grpc/grpc-go/issues/3206
// It means we should use as default for every client connection. So it's better to put in global config here.
var defaultgRPCKeepAliveClientParam = keepalive.ClientParameters{
	Time:                30 * time.Second, // send pings every 30 seconds if there is no activity
	Timeout:             time.Second,      // wait 1 second for ping ack before considering the connection dead
	PermitWithoutStream: true,             // send pings even without active streams
}

func GetDefaultgRPCKeepAliveClientParam() keepalive.ClientParameters {
	return defaultgRPCKeepAliveClientParam
}

var (
	Server           serverConfig
	PetStoreClient   petStoreClientConfig
	GRPCHealthClient gRPCHealthClientConfig
)

func init() {
	// init server
	flag.StringVar(&Server.GRPCPort, "grpc-port", lookupEnvOrString("GRPC_PORT", "9099"), "port to serve gRPC service (default 9099)")
	flag.StringVar(&Server.RESTPort, "rest-port", lookupEnvOrString("REST_PORT", "8889"), "port to serve REST service (default 8889)")
	flag.BoolVar(&Server.AllowCORS, "allow-cors", lookupEnvOrBool("ALLOW_CORS", false), "enable if want to allow CORS")
	flag.IntVar(&Server.GracefulStopTimeout, "gracefulstop-timeout", lookupEnvOrInt("GRACEFULSTOP_TIMEOUT", 30), "timeout for gracefulstop server")
	flag.IntVar(&Server.CmuxReadTimeout, "cmuxread-timeout", lookupEnvOrInt("CMUXREAD_TIMEOUT", 5), "timeout for cmux read listener on port")

	// init petstore url
	flag.StringVar(&PetStoreClient.URL, "petstore-url", lookupEnvOrString("PETSTORE_URL", "http://petstore.swagger.io/v2"), "PetStore URL to connect to in v3")

	// init grpc health client
	flag.StringVar(&GRPCHealthClient.Addr, "grpc-healthclient-serveraddr", lookupEnvOrString("GRPC_HEALTHCLIENT_SERVERADDR", "localhost:9099"), "Server Address to connect to gRPC Health service")

	// need to explicitly call flag.Parse() in main() to parse all parameters into variables.
	// cannot put in init() here, otherwise `go test` will be failed.
	// ref: https://github.com/golang/go/issues/31859, https://golang.org/pkg/flag/
}

// implements 12-factors technic by function lookupEnvOrXXX in this boilerplate.
// We don't use viper or cleanenv here because we want this boilerplate clean,
// and we want to make user easier to change to their preferred configuration management library.
// ref: https://www.gmarik.info/blog/2019/12-factor-golang-flag-package/
func lookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func lookupEnvOrInt(key string, defaultVal int) int {
	if val, ok := os.LookupEnv(key); ok {
		v, err := strconv.Atoi(val)
		if err == nil {
			return v
		}
	}
	return defaultVal
}

func lookupEnvOrBool(key string, defaultVal bool) bool {
	if val, ok := os.LookupEnv(key); ok {
		if val != "" {
			return true
		}
	}
	return defaultVal
}
