package service

import (
	"context"

	pb "gitlab.com/wongsatorn.tho/pingpong-go-grc-table/pkg/proto/tagthai/service"

	"github.com/golang/protobuf/ptypes/empty"
)

type healthCheckServiceServerImpl struct {
	pb.UnimplementedHealthCheckServiceServer
}

func NewHealthCheckServiceServer() pb.HealthCheckServiceServer {
	return &healthCheckServiceServerImpl{}
}

func (h *healthCheckServiceServerImpl) Health(ctx context.Context, empty *empty.Empty) (*pb.HealthCheck, error) {
	response := &pb.HealthCheck{
		Status: "OK",
	}
	return response, nil
}
