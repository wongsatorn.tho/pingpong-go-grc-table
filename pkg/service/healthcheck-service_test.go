package service

import (
	"context"
	"reflect"
	"testing"

	"github.com/golang/protobuf/ptypes/empty"
	pb "gitlab.com/wongsatorn.tho/pingpong-go-grc-table/pkg/proto/tagthai/service"
)

func Test_healthCheckServiceServerImpl_Health(t *testing.T) {
	type args struct {
		ctx   context.Context
		empty *empty.Empty
	}
	tests := []struct {
		name    string
		args    args
		want    *pb.HealthCheck
		wantErr bool
	}{
		{
			"check return OK",
			args{nil, nil},
			&pb.HealthCheck{Status: "OK"},
			false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &healthCheckServiceServerImpl{}
			got, err := h.Health(tt.args.ctx, tt.args.empty)
			if (err != nil) != tt.wantErr {
				t.Errorf("Health() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Health() got = %v, want %v", got, tt.want)
			}
		})
	}
}
