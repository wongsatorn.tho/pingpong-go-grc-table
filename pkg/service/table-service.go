package service

import (
	"context"
	"math/rand"

	"gitlab.com/wongsatorn.tho/pingpong-go-grc-table/pkg/proto/tagthai/service"
	pb "gitlab.com/wongsatorn.tho/pingpong-go-grc-table/pkg/proto/tagthai/service"
)

type TableServiceServerImpl struct {
	pb.TableServiceServer
}

func NewTableServiceServer() pb.TableServiceServer {
	return &TableServiceServerImpl{}
}

func (s *TableServiceServerImpl) Table(c context.Context, Ball *service.Ball) (*service.Ball, error) {
	tableFactor := (70.0 + rand.Float64()*20) / 100.0

	return &service.Ball{BallPower: Ball.BallPower * tableFactor}, nil
}
