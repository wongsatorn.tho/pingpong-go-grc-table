package server

import (
	"context"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/wongsatorn.tho/pingpong-go-grc-table/pkg/configs"
	gw "gitlab.com/wongsatorn.tho/pingpong-go-grc-table/pkg/proto/tagthai/service"
	"gitlab.com/wongsatorn.tho/pingpong-go-grc-table/pkg/service"

	"github.com/go-chi/chi"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/soheilhy/cmux"
	httpSwagger "github.com/swaggo/http-swagger"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

// run both gRPC server and HTTP REST server
func RunServer() error {
	// Use zap logger because it's fast, but it can be changed to any logger that gRPC middleware support
	// https://github.com/uber-go/zap#performance
	zapLogger, _ := zap.NewProduction()
	defer zapLogger.Sync() // flushes buffer, if any
	log := zapLogger.Sugar()
	log.Info("Start gRPC with port:", configs.Server.GRPCPort, ", and REST with port:", configs.Server.RESTPort)

	grpcPort := ":" + configs.Server.GRPCPort
	restPort := ":" + configs.Server.RESTPort

	// create listener for gRPC and REST/Website
	var grpcL, httpL net.Listener

	// if we use the same port, then we multiplex 2 protocols into one using cmux lib
	var m cmux.CMux
	if grpcPort == restPort {
		listen, err := net.Listen("tcp", restPort)
		if err != nil {
			log.Error("Cannot bind to port ", restPort)
			log.Panic(err)
		}

		// Create the cmux object that will multiplex 2 protocols on the same port.
		// The two following listeners will be served on the same port below gracefully.
		// https://www.clarifai.com/blog/muxing-together-grpc-and-http-traffic-with-grpc-gateway
		// Beware that TCP KeepAlive across protocol will not possible as explain here: https://github.com/soheilhy/cmux/issues/62
		m = cmux.New(listen)
		// Need to set timeout as workaround for gracefulStop per open issue here: https://github.com/soheilhy/cmux/issues/76
		m.SetReadTimeout(time.Duration(configs.Server.CmuxReadTimeout) * time.Second)
		// Match gRPC requests here, there's issue that break default connection, so we need workaround here
		// https://github.com/soheilhy/cmux/issues/64
		grpcL = m.MatchWithWriters(cmux.HTTP2MatchHeaderFieldSendSettings("content-type", "application/grpc"))
		// Otherwise match regular http requests.
		httpL = m.Match(cmux.Any())
		// another option to match gRPC per issue above
		// httpL = m.Match(cmux.HTTP1())
		// grpcL = m.Match(cmux.Any())

		log.Info("Starting gRPC and REST/Web on the same port ", restPort)
	} else {
		// if we use different port, then just listen differently
		var err error
		grpcL, err = net.Listen("tcp", grpcPort)
		if err != nil {
			log.Error("Cannot bind to port ", grpcPort)
			log.Panic(err)
		}
		httpL, err = net.Listen("tcp", restPort)
		if err != nil {
			log.Error("Cannot bind to port ", restPort)
			log.Panic(err)
		}
	}

	// context can change to timeout or cancel for temporary start or cancel mechanic
	ctx := context.Background()

	// separate channel for server error and for signal stop
	errChan := make(chan error)
	stopChan := make(chan os.Signal)

	// bind OS events to the signal channel
	signal.Notify(stopChan, syscall.SIGTERM, syscall.SIGINT)

	// ==================== start gRPC server ====================
	// run blocking call in a separate goroutine, report errors via channel
	var gs *grpc.Server
	go func() {
		// Create gRPC with should-be-used middleware options
		gs = grpc.NewServer(
			grpc_middleware.WithUnaryServerChain(
				grpc_ctxtags.UnaryServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
				grpc_zap.UnaryServerInterceptor(zapLogger),
				RequestIdInjectServerInterceptor(),
				grpc_recovery.UnaryServerInterceptor(),
			),
		)

		gw.RegisterHealthCheckServiceServer(gs, service.NewHealthCheckServiceServer())
		gw.RegisterTableServiceServer(gs, service.NewTableServiceServer())

		log.Info("Start gRPC server on port ", grpcPort, ", at ", time.Now())
		if err := gs.Serve(grpcL); err != nil {
			errChan <- err
		}
	}()

	// ==================== start HTTP REST gateway server ====================
	var hs *http.Server
	go func() {
		// initial mux gRPC-gateway with default options
		// can add custom options here: https://godoc.org/github.com/grpc-ecosystem/grpc-gateway/runtime#ServeMuxOption
		gwmux := runtime.NewServeMux()

		// use withBlock() to wait until gRPC service is up before start gateway server
		opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBlock()}

		// TODO register gRPC endpoints to REST gateway server here
		if err := gw.RegisterHealthCheckServiceHandlerFromEndpoint(ctx, gwmux, "0.0.0.0"+grpcPort, opts); err != nil {
			errChan <- err
		}
		if err := gw.RegisterTableServiceHandlerFromEndpoint(ctx, gwmux, "0.0.0.0"+grpcPort, opts); err != nil {
			errChan <- err
		}

		// Serve the swagger, and serve gRPC-gateway mux at the same port, but different path
		r := chi.NewRouter()
		r.Handle("/*", gwmux)

		// because our swagger-ui default load "/swagger.json", so we copy this file here in Makefile
		r.HandleFunc("/api/v1/swagger/doc.json", func(w http.ResponseWriter, r *http.Request) {
			http.ServeFile(w, r, "assets/swagger.json")
		})

		r.Get("/api/v1/swagger/*", httpSwagger.Handler())

		// custom flag for enable CORS for swagger
		var h http.Handler
		h = r
		if configs.Server.AllowCORS {
			h = allowCORS(r)
		}

		// initial HTTP server to serve Swagger-UI, gRPC-gateway, and Website
		hs = &http.Server{
			Handler: h,
		}

		log.Info("Start gateway server on port ", restPort, ", at ", time.Now())
		if err := hs.Serve(httpL); err != nil {
			errChan <- err
		}
	}()

	// start cmux if we want to run on the same port
	if grpcPort == restPort && m != nil {
		log.Info("cmux Serve()")
		go m.Serve()
	}

	// ==================== Graceful Shutdown Block ====================
	// terminate your environment gracefully before leaving main function
	// Should shutdown gRPC-gateway before gRPC server
	defer func() {
		t := time.Now()

		// graceful shutdown REST/Web server
		if hs != nil {
			t1 := time.Now()
			log.Info("Graceful Shutting down Gateway server...", t1)
			if err := hs.Shutdown(ctx); err != nil {
				log.Error("Error when shutdown Gateway server ", err)
			}
			t2 := time.Now()
			log.Info("Done shutdown Gateway server in ", t2.Sub(t1))
		}

		// graceful shutdown gRPC server
		if gs != nil {
			t1 := time.Now()
			log.Info("Graceful shutting down gRPC server...", t1)
			stopped := make(chan bool)
			go func() {
				gs.GracefulStop()
				stopped <- true
			}()
			// need to force shutdown if graceful takes too long. For case that we use cmux on the same port, it's possible to happen
			// see open issue here: https://github.com/soheilhy/cmux/issues/76
			select {
			case <-time.After(time.Duration(configs.Server.GracefulStopTimeout) * time.Second):
				log.Info("Timeout when trying to shutdown gRPC")
				if err := grpcL.Close(); err != nil {
					log.Error("Error when shutdown gRPC listener ", err)
				}
			case <-stopped:
			}

			t2 := time.Now()
			log.Info("Done shutdown gRPC server in ", t2.Sub(t1))
		}

		//closeDbConnections()

		log.Info("Shutdown done in ", time.Now().Sub(t))
	}()

	// ==================== Signal block ====================
	// block until either context, OS signal, or server fatal error
	select {
	case err := <-errChan:
		log.Infof("error at server: %v\n", err)
		break
	case <-stopChan:
		log.Info("Stop channel selected")
		break
	case <-ctx.Done():
		log.Info("Context Done selected")
		break
	}

	return nil
}
