/*
This folder use for store .proto file for automatically generate using Makefile.
Please don't put swagger spec (*.json) file here because Makefile will delete it before re-generate the new one from proto file.

How to use

Place your proto files here, run make gen, and the *.swagger.json will be created here.
*/

package server
