GO_INSTALLED := $(shell which go)
PROTOC_INSTALLED := $(shell which protoc)
PROTOC_GEN_GO_INSTALLED := $(shell which protoc-gen-go)
PROTOC_GEN_GO_GRPC_INSTALLED := $(shell which protoc-gen-go-grpc)
PROTOC_GEN_SWAGGER_INSTALLED := $(shell which protoc-gen-openapiv2)
PROTOC_GEN_GRPC_GATEWAY_INSTALLED := $(shell which protoc-gen-grpc-gateway)

GO_SWAGGER_INSTALLED := $(shell which swagger)
OAPI_CODEGEN_INSTALLED := $(shell which oapi-codegen)

GO_FILES = $(shell go list ./... | grep -v /vendor/ | grep -v /api/ | grep -v /cmd/)

OPENAPI_V2_PATTERN_JSON := "^ *\"swagger\": \"?2\.0\"?"
OPENAPI_V2_PATTERN_YAML := "^swagger: ['\"]?2\.0['\"]?"
OPENAPI_V3_PATTERN_JSON := "^ *\"openapi\": \"?3\.0"
OPENAPI_V3_PATTERN_YAML := "^openapi: ['\"]?3\.0"

all: gen fmt vet test ## Run generate code and fmt, then vet, then test

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

upgrade-protobuf: ## Upgrade protobuf by brew
	brew update
	brew upgrade protobuf

upgrade-modules: ## Upgrade all go modules to the latest version (-d to prevent create binary, will be default in newer go version)
	go get -d -u ./...

upgrade: upgrade-modules tidy install ## Upgrade all go modules to the latest version and install required binaries

install: ## Install required binaries
	go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway
	go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2
	go install google.golang.org/protobuf/cmd/protoc-gen-go
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
	go install github.com/go-swagger/go-swagger/cmd/swagger
	go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen

tidy-vendor tidy: ## Vendor and Tidy go modules
	@rm -rf vendor
	@go mod vendor
	@go mod tidy

check: ## Check if all required binaries are installed correctly
ifndef GO_INSTALLED
	$(error "go is not installed, please run 'brew install go'")
endif
ifndef PROTOC_INSTALLED
	$(error "protoc is not installed, please run 'brew install protobuf'")
endif
ifndef PROTOC_GEN_GO_INSTALLED
	$(error "protoc-gen-go is not installed, please check GOPATH/bin")
endif
ifndef PROTOC_GEN_GO_GRPC_INSTALLED
	$(error "protoc-gen-go-grpc is not installed, please check GOPATH/bin")
endif
ifndef PROTOC_GEN_SWAGGER_INSTALLED
	$(error "protoc-gen-openapiv2 is not installed, please check GOPATH/bin")
endif
ifndef PROTOC_GEN_GRPC_GATEWAY_INSTALLED
	$(error "protoc-gen-grpc-gateway is not installed, please check GOPATH/bin")
endif
ifndef GO_SWAGGER_INSTALLED
	$(error "swagger is not installed, please check GOPATH/bin")
endif
ifndef OAPI_CODEGEN_INSTALLED
	$(error "oapi-codegen is not installed, please check GOPATH/bin")
endif

gen: check ## Run code generator, this generate server and clients code from spec in /api/ folder
#cleanup
	@rm -rf pkg/proto/kbtg/*
	@rm -rf api/server/*.json
	@rm -f web/swagger.json
#generate server
	@protoc \
		-I. \
		-I./third_party/googleapis/ \
		-I./vendor/github.com/grpc-ecosystem/grpc-gateway/v2/ \
		--go_out ./pkg/proto/ \
		--go-grpc_out ./pkg/proto/ \
		--grpc-gateway_out ./pkg/proto/ \
		--grpc-gateway_opt logtostderr=true \
		--openapiv2_out ./ \
		--openapiv2_opt logtostderr=true \
		api/server/*.proto
	@cp api/server/*.swagger.json web/swagger.json
#go generate if any requires
	@go generate ./...
	@go mod vendor
	@go mod tidy
	@echo "Success! Generated server and client"

build: gen ## Build application binary
	@rm -rf bin
	@mkdir -p bin
	@go build -o bin/server cmd/server/*.go
	@echo "Success! Binaries can be found in 'bin' dir"

run: ## Run application on default port 8889/9099 and print console format
	@go run cmd/server/main.go

vet: ## Run go vet
	@go vet $(GO_FILES)

fmt: ## Run go fmt
	@go fmt $(GO_FILES)

test: ## Run go test with coverage and race detection
	@go test $(GO_FILES) -cover --race
